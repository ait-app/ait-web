import React from "react";
import ReactDOM from "react-dom/client";

import { Provider as ReduxProvider } from "react-redux";

import App from "src/App";

import { store } from "src/redux/storage";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <ReduxProvider store={store}>
    <App />
  </ReduxProvider>
);
