import { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import {
  AppBar,
  Toolbar,
  Box,
  Grid,
  Typography,
  Container,
  IconButton,
  Dialog,
  DialogContent,
} from "@mui/material";

import { Person, Logout, AutoFixHigh, Home } from "@mui/icons-material";

import { FormsComponent } from "src/components";
import { AuthServices } from "src/services";
import { Register } from "src/assets";

import { setUserToken, unsetUserToken } from "src/redux/user/actions.user";
import {
  setUserSession,
  unsetUserSession,
} from "src/redux/session/actions.session";

const Navbar = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [authDialog, setAuthDialog] = useState(false);
  const [way, setWay] = useState("login");

  const session = useSelector((state) => state.session);

  const logout = () => {
    dispatch(unsetUserToken());
    dispatch(unsetUserSession());

    history.push("/");
  };

  const login = async (callback) => {
    try {
      const data = await AuthServices.Login(callback);

      dispatch(setUserToken(data.id));
      dispatch(setUserSession());

      setAuthDialog(false);

      history.push("/client");
    } catch (error) {
      alert(error.message);
    }
  };

  const register = async (callback) => {
    try {
      const data = await AuthServices.Register(callback);

      dispatch(setUserToken(data.id));
      dispatch(setUserSession());

      setAuthDialog(false);

      history.push("/client");
    } catch (error) {
      alert(error.message);
    }
  };

  const publicButtons = [
    {
      onClick: () => window.location.replace("https://amirhossein.info"),
      icon: <Home />,
    },
    {
      onClick: () => history.push("/about"),
      icon: <AutoFixHigh />,
    },
    {
      onClick: () => (session ? history.push("/client") : setAuthDialog(true)),
      icon: <Person />,
    },
  ];

  const privateButtons = [
    {
      onClick: logout,
      icon: <Logout />,
    },
  ];

  return (
    <Box>
      <AppBar elevation={0} position="absolute">
        <Container>
          <Toolbar>
            <Typography
              variant="h5"
              color="white"
              onClick={() => history.push("/")}
              sx={{ flexGrow: 1, cursor: "pointer" }}
            >
              Amirhossein.Info Todo
            </Typography>
            {publicButtons.map((button) => (
              <IconButton onClick={button.onClick} sx={{ color: "white" }}>
                {button.icon}
              </IconButton>
            ))}

            {session &&
              privateButtons.map((button) => (
                <IconButton onClick={button.onClick} sx={{ color: "white" }}>
                  {button.icon}
                </IconButton>
              ))}
          </Toolbar>
        </Container>
      </AppBar>
      <Toolbar />

      <Dialog
        open={authDialog}
        onClose={() => setAuthDialog(false)}
        maxWidth="md"
        fullWidth
      >
        <DialogContent>
          <Grid spacing={3} container>
            <Grid md={6} item>
              <Typography variant="h4" color="primary" gutterBottom>
                {way === "login" ? "Signin" : "Create account"}
              </Typography>
              <FormsComponent
                name={way}
                button={way === "login" ? "Signin" : "Create my account"}
                callback={way === "login" ? login : register}
              />
              <Typography
                variant="body1"
                onClick={() => setWay(way === "login" ? "register" : "login")}
                sx={{
                  cursor: "pointer",
                  "&:hover": {
                    color: "primary.main",
                  },
                }}
              >
                {way === "login" ? "I don't have account" : "I have account"}
              </Typography>
            </Grid>
            <Grid md={6} item>
              <Box component="img" src={Register} />
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    </Box>
  );
};

export default Navbar;
