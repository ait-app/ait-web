import { useForm } from "react-hook-form";

import { Box, TextField, Button } from "@mui/material";

import forms from "src/config/forms";

const FormsComponent = ({ name, button, def, callback }) => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    defaultValues: def,
  });

  const form = forms[name];

  const onSubmit = (data) => callback(data);

  return (
    <Box>
      {Object.entries(form).map(([name, field]) => {
        switch (field.type) {
          default:
            return (
              <TextField
                key={name}
                {...register(name)}
                label={field.label}
                type={field.type}
                placeholder={field.placeholder}
                margin="normal"
                fullWidth
              />
            );
        }
      })}
      <Button
        variant="contained"
        color="primary"
        size="large"
        onClick={handleSubmit(onSubmit)}
        sx={{ color: "white", mt: 1 }}
        disableElevation
      >
        {button}
      </Button>

      <br />
      <br />
    </Box>
  );
};

export default FormsComponent;
