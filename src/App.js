import {
  ThemeProvider,
  createTheme,
  CssBaseline,
  colors,
  Box,
} from "@mui/material";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { HomePage, ClientPage, AboutPage } from "src/pages";
import { Navbar } from "src/components";

function App() {
  const routes = [
    {
      path: "/",
      component: <HomePage />,
    },
    {
      path: "/client",
      component: <ClientPage />,
    },
    {
      path: "/about",
      component: <AboutPage />,
    },
  ];

  const theme = createTheme({
    palette: {
      background: {
        default: colors.cyan[50],
      },
      primary: {
        main: colors.cyan[500],
      },
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router>
        <Navbar />
        <Box>
          <Switch>
            {routes.map((route) => (
              <Route key={route.path} path={route.path} exact>
                {route.component}
              </Route>
            ))}
          </Switch>
        </Box>
      </Router>
    </ThemeProvider>
  );
}

export default App;
