import Login from "src/assets/images/login.svg";
import Register from "src/assets/images/register.svg";
import Wave from "src/assets/images/wave.svg";

// import Development from "src/assets/images/development.jpg";
import Developer from "src/assets/images/developer.png";

export { Login, Register, Wave, Developer };
