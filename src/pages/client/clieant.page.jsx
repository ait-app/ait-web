import { useState, useEffect } from "react";

import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { formatDistanceToNow } from "date-fns";

import {
  Container,
  Typography,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  ListItemIcon,
} from "@mui/material";

import { Circle } from "@mui/icons-material";

import { TodoServices } from "src/services";
import { FormsComponent } from "src/components";

const colors = [
  {
    status: 0,
    color: "error",
    hist: "Added, no action yet.",
  },
  {
    status: 1,
    color: "info",
    hist: "On working, doing it!",
  },
  {
    status: 2,
    color: "success",
    hist: "Done, you did it!",
  },
];

const ClientPage = () => {
  const history = useHistory();

  const session = useSelector((state) => state.session);

  !session && history.push("/");

  const uid = useSelector((state) => state.user);

  const [todos, setTodos] = useState([]);

  const [updateId, setUpdateId] = useState("");
  const [updateItem, setUpdateItem] = useState({});

  const fetchData = async () => {
    try {
      const data = await TodoServices.By(uid);

      setTodos(data.reverse());
    } catch (error) {
      alert(error.message);
    }
  };

  useEffect(() => {
    session && fetchData();
  }, []);

  const insert = async (callback) => {
    const sendingData = {
      ...callback,
      status: 0,
      user: uid,
    };

    try {
      await TodoServices.Create(sendingData);

      fetchData();
    } catch (error) {
      alert(error.message);
    }
  };

  const update = async (callback) => {
    try {
      await TodoServices.Update(updateId, callback);

      setUpdateId("");
      setUpdateItem({});

      fetchData();
    } catch (error) {
      alert(error.message);
    }
  };

  const readyUpdate = (id) => {
    setUpdateId("");
    setUpdateItem({});

    const selected = todos.filter((todo) => todo._id === id)[0];
  };

  const color = (status) => {
    return colors[status].color;
  };

  return (
    <Container sx={{ py: 2 }}>
      <Grid spacing={3} container>
        <Grid md={4} item>
          <Typography variant="h4" color="primary" gutterBottom>
            Add task
          </Typography>
          <FormsComponent
            name="add_task"
            button="Add this task"
            callback={insert}
          />
          <Divider />
          <List>
            {colors.map((color) => (
              <ListItem key={color.status}>
                <ListItemText primary={color.hist} />
                <ListItemIcon>
                  <Circle color={color.color} />
                </ListItemIcon>
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid md={4} item>
          <Typography variant="h4" color="primary" gutterBottom>
            All tasks
          </Typography>
          {todos ? (
            <List>
              {todos.map((todo) => (
                <ListItem key={todo._id} disablePadding>
                  <ListItemButton onClick={() => readyUpdate(todo._id)}>
                    <ListItemText
                      primary={todo.title}
                      secondary={
                        formatDistanceToNow(new Date(todo.createdAt)) + " ago"
                      }
                    />
                    <ListItemIcon>
                      <Circle color={color(todo.status)} />
                    </ListItemIcon>
                  </ListItemButton>
                </ListItem>
              ))}
            </List>
          ) : (
            <p>Loading</p>
          )}
        </Grid>
        <Grid md={4} item>
          <Typography variant="h4" color="primary" gutterBottom>
            Edit task
          </Typography>
          {updateId ? (
            <FormsComponent
              name="add_task"
              button="Update this task"
              def={updateItem}
              callback={update}
            />
          ) : (
            <Typography variant="body1">Select one todo.</Typography>
          )}
        </Grid>
      </Grid>
    </Container>
  );
};

export default ClientPage;
