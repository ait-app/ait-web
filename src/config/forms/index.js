const forms = {
  login: {
    email: {
      label: "Email",
      placeholder: "Enter your Email",
      type: "email",
    },
    password: {
      label: "Password",
      placeholder: "Enter your Password",
      type: "password",
    },
  },
  register: {
    name: {
      label: "Name",
      placeholder: "Enter your Name",
      type: "text",
    },
    email: {
      label: "Email",
      placeholder: "Enter your Email",
      type: "email",
    },
    password: {
      label: "Password",
      placeholder: "Enter your Password",
      type: "password",
    },
  },
  add_task: {
    title: {
      label: "Title",
      placeholder: "Enter your task Title",
      type: "text",
    },
    caption: {
      label: "Caption",
      placeholder: "Enter your task Caption",
      type: "caption",
    },
  },
};

export default forms;
