const env = process.env;

const config = {
  axiosBaseUrl: env.REACT_APP_URL,
};

export default config;
