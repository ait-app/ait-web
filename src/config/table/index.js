const tables = {
  forms: {
    title: "لیست فرم ها",
    fields: {
      row: "ردیف",
      name: "نام فرم",
      _id: "شماره فرم",
    },
  },
};

export default tables;
