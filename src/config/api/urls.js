const urls = {
  todo: "/todo",
  user: "/user",
  auth: {
    resgister: "/auth/register",
    login: "/auth/login",
  },
};

export default urls;
