import axios from "axios";

import config from "src/config";
import urls from "src/config/api/urls";

export const URLs = urls;
export const API = axios.create({
  baseURL: config.axiosBaseUrl,
});
