import { API, URLs } from "src/config/api";

const { auth } = URLs;

export const Register = async (body) => {
  try {
    const response = await API.post(auth.resgister, body);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};

export const Login = async (body) => {
  try {
    const response = await API.post(auth.login, body);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};
