import { API, URLs } from "src/config/api";

const { todo } = URLs;

export const All = async () => {
  try {
    const response = await API.get(todo);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};

export const By = async (user) => {
  try {
    const response = await API.get(`${todo}/by/${user}`);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};

export const Single = async (id) => {
  try {
    const response = await API.get(`${todo}/${id}`);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};

export const Create = async (body) => {
  try {
    const response = await API.post(todo, body);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};

export const Delete = async (id) => {
  try {
    const response = await API.delete(`${todo}/${id}`);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};

export const Update = async (id, body) => {
  try {
    const response = await API.put(`${todo}/${id}`, body);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};
