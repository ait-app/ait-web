import { API, URLs } from "src/config/api";

const { user } = URLs;

export const All = async () => {
  try {
    const response = await API.get(user);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};

export const Single = async (id) => {
  try {
    const response = await API.get(`${user}/${id}`);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};

export const Delete = async (id) => {
  try {
    const response = await API.delete(`${user}/${id}`);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};

export const Update = async (id, body) => {
  try {
    const response = await API.put(`${user}/${id}`, body);

    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error.response.data);
  }
};
