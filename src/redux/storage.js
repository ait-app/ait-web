import { createStore } from "redux";

import { saveState, loadState } from "src/redux/localstorage";
import { allReducers } from "src/redux/reducers";

const presentedState = loadState();

export const store = createStore(
  allReducers,
  presentedState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

store.subscribe(() =>
  saveState({
    user: store.getState().user,
    session: store.getState().session,
  })
);
