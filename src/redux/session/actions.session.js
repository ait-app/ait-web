export const setUserSession = () => {
  return {
    type: "SET_SESSION",
  };
};

export const unsetUserSession = () => {
  return {
    type: "UNSET_SESSION",
  };
};
