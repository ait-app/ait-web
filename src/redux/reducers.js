import { combineReducers } from "redux";

import { userReducer } from "src/redux/user/reducers.user";
import { sessionReducer } from "src/redux/session/reducers.session";

export const allReducers = combineReducers({
  user: userReducer,
  session: sessionReducer,
});
